﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealItem : MonoBehaviour
{
    PlayerHealth playerHealth;
    public int heal;

    private void Start()
    {
        playerHealth = FindObjectOfType<PlayerHealth>();
    }

    private void OnCollisionEnter(Collision playerHeal)
    {
        if (playerHeal.gameObject.tag == "Player")
        {
            Healing(heal);
            
            Destroy(gameObject);
        }
    }

    public void Healing(int heal)
    {
        if (playerHealth.healthSlider.value >= 100)
        {
            playerHealth.currentHealth = 100;
        }
        else
        {
            playerHealth.currentHealth += heal;

            playerHealth.healthSlider.value = playerHealth.currentHealth;
        }
    }
}
