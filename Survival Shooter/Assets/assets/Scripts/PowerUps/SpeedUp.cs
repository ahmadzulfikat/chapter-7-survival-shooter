﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedUp : MonoBehaviour
{
    PlayerMovement playerMovement;
    public int timer;
    public int speedPlus;

    MeshRenderer speedItemRenderer;
    SphereCollider itemCollider;
    public GameObject halo;

    private void Start()
    {
        playerMovement = FindObjectOfType<PlayerMovement>();
        speedItemRenderer = GetComponent<MeshRenderer>();
        itemCollider = GetComponent<SphereCollider>();
    }

    private void OnCollisionEnter(Collision playerSpeed)
    {
        if(playerSpeed.gameObject.tag == "Player")
        {
            playerMovement.speed = speedPlus;
            speedItemRenderer.enabled = false;
            itemCollider.enabled = false;
            Destroy(halo);

            StartCoroutine(PowerUpTimer(timer));
        }
    }

    IEnumerator PowerUpTimer(int time)
    {
        yield return new WaitForSeconds(time);
        playerMovement.speed = 6;
        Destroy(gameObject);
    }
}
