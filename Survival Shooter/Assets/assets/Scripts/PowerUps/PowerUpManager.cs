﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpManager : MonoBehaviour
{
    public GameObject[] itemPrefabs;
    public float timer;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnItems", timer, timer);
    }

    void SpawnItems()
    {
        float randomX = Random.Range(-16f, 16f);
        float randomZ = Random.Range(-16f, 16f);

        int randomItems = Random.Range(0, itemPrefabs.Length);

        Instantiate(itemPrefabs[randomItems], new Vector3(randomX, 1, randomZ), Quaternion.identity);
    }
}
