﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFactory : MonoBehaviour, IFactory
{
    [SerializeField]
    public GameObject[] enemyPrefab;
    public Transform[] spawnPos;

    public GameObject FactoryMethod(int tag, int pos)
    {
        GameObject enemy = Instantiate(enemyPrefab[tag], spawnPos[pos].position, Quaternion.identity);
        return enemy;
    }
}

